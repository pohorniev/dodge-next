import Link from "next/link"
import { NavContainer, Logo, NavList, NavItem, NavSocial } from "./navigation.styles";

const Nav = () => (
    <NavContainer>
      <div className="container">
        <Logo>
          <img src="/images/logo.svg" alt=""/>
        </Logo>

        <NavList>
          <NavItem>
            <Link href={'/dodge'}><a>Dodge</a></Link>
            <div className="line"/>
          </NavItem>
          <NavItem active>
            <Link href={'/ram'}><a>RAM</a></Link>
            <div className="line"/>
          </NavItem>
          <NavItem>
            <Link href={'/oferta'}><a>Oferta</a></Link>
            <div className="line"/>
          </NavItem>
          <NavItem>
            <Link href={'/salon'}><a>Salon</a></Link>
            <div className="line"/>
          </NavItem>
          <NavItem>
            <Link href={'/serwis'}><a>Serwis</a></Link>
            <div className="line"/>
          </NavItem>
          <NavItem>
            <Link href={'/kontakt'}><a>Kontakt</a></Link>
            <div className="line"/>
          </NavItem>
        </NavList>

        <NavSocial>
          <a href={'#'} target="_blank" rel="nofollow">
            <img src="/images/icons/icon-fb.svg" alt=""/>
          </a>
          <a href={'#'} target="_blank" rel="nofollow">
            <img src="/images/icons/icon-instagram.svg" alt=""/>
          </a>
          <a href={'#'} target="_blank" rel="nofollow">
            <img src="/images/icons/icon-rect.svg" alt=""/>
          </a>
        </NavSocial>
      </div>

    </NavContainer>
)

export default Nav