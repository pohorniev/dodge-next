export const sizes = {
  xxs: 25,
  xs: 30,
  phone: 37.5,
  tabPort: 50,
  tabLand: 75,
  bigDesktop: 87.5
}

export const breakpoints = key => {
 if (key == 'bigDesktop') {
   return style => `@media (min-width: ${sizes[key]}em) { ${style} }`
 } else {
   return style => `@media (max-width: ${sizes[key]}em) { ${style} }`
 }
}