import Head from 'next/head'
import Nav from "./navigation/navigation";
import Footer from "./footer/footer";

const Layout = ({title, children}) => {
  return (
      <div>
        <Head>
          <title>{title} | Dodge</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link rel="shortcut icon" href="/images/logo_xs.png" />

          <link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@400;500&family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet" />
        </Head>
        <Nav />
        {children}
        <Footer/>
      </div>
  )
}

export default Layout;