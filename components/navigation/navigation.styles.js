import styled from "styled-components";
import { breakpoints } from "../../styles/breakpoints.styles";

export const NavContainer = styled.nav`
  width: 100%;
  height: 16rem;
  background-color: black;
  
  
  .container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 100%;
  }
  
  // ${breakpoints('phone') `
  //   height: 6rem
  // `} 
`;

export const Logo = styled.div`
  img {
      width: 26rem;
      
      ${breakpoints('bigDesktop') `
        width: 32rem
      `} 
    }
`;

export const NavList = styled.ul`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  list-style: none;
  width: 50%;
  height: 100%;
  
  ${breakpoints('tabPort') `
    display: none;
  `} 
`;

export const NavItem = styled.li`
  //width: 24rem;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 2rem;
  
  a {
    color: ${props => props.active ? props.theme.colors.primary : 'white'};
    font-family: ${props => props.theme.fonts.heading};
    font-size: 1.8rem;
    text-transform: uppercase;
    margin-right: 2rem;
    transition: color .3s;
    
    &:hover { color: ${props => props.theme.colors.primary}};
    
    &:hover ~ .line {
      background-color: ${props => props.theme.colors.primary};
    }
  }
  
  .line {
    width: 4.4rem;
    height: 1px;
    background-color: ${props => props.active ? props.theme.colors.primary : 'white'};
    transition: background-color .3s;
  }
`;

export const NavSocial = styled.div`
  display: flex;
  flex-direction: column;
  
  a:not(:last-child) {
    margin-bottom: 1.6rem;
  }
`;