import styled from "styled-components";

export const Title = styled.h2`
  color: ${props => props.theme.colors.primary};
  font-size: 3.4rem;
  font-weight: 400;
  letter-spacing: 2px;
  text-align: center;
  text-transform: uppercase;
  padding-bottom: 1rem;
  margin-bottom: 3rem;
  position: relative;
  
  &:before {
    content: '';
    position: absolute;
    bottom: 0;
    right: 0;
    width: 300rem;
    height: 2px;
    background-color: ${props => props.theme.colors.primary};
  }
`;

export const Text = styled.div`
  color: ${props => props.theme.colors.text};
  font-size: 1.6rem;
  line-height: 1.5;
  text-align: center;
  width: 80rem;
  margin-bottom: 8rem;
  
  h3 {
    margin-bottom: 1.6rem;
  }
  
  p {
    margin-bottom: 2rem;
  }
`;