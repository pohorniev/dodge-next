import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
  margin: 0;
  padding: 0;
}

*,
*::before,
*::after {
  box-sizing: inherit;
}

.clearfix::after {
  content: "";
  display: table;
  clear: both;
}

html {
  height: 100%;
  box-sizing: border-box;
  font-size: 62.5%; // 1rem = 10px, 10px/16px = 62.5%
}

body {
  height: 100%;
  font-family: ${props => props.theme.fonts.primary};
  //font-family: 'Cabin', sans-serif;
  position: relative;
  overflow-x: hidden;
  //background-color: $color-grey-cloud;
}

a,
a:hover {
  color: black;
  text-decoration: none;
}

.container {
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}

@media (min-width: 576px) {
  .container {
    max-width: 540px;
  }
}

@media (min-width: 768px) {
  .container {
    max-width: 720px;
  }
}

@media (min-width: 992px) {
  .container {
    max-width: 960px;
  }
}

@media (min-width: 1200px) {
  .container {
    max-width: 1140px;
  }
}
`;

export default GlobalStyle;