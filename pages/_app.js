import App from 'next/app'
import { ThemeProvider } from 'styled-components'
import GlobalStyle from "../styles/global.styles";

const theme = {
  colors: {
    primary: '#D60000',
    text: '#686670',
  },
  fonts: {
    primary: `"Opens Sans", sans-serif`,
    heading: `"Barlow Condensed", sans-serif`
  }
}

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
        <ThemeProvider theme={theme}>
          <GlobalStyle/>
          <Component {...pageProps} />
        </ThemeProvider>
    )
  }
}