import styled from "styled-components"

export const HeaderContainer = styled.header`
  width: 100%;
  height: calc(100vh - 16rem);
`;

export const Slide = styled.div`
  width: 100%;
  height: 100%;
  
  .container {
    position: relative;
  }
`;

export const SlideImage = styled.div`
  width: 100%;
  height: 100%;
  
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export const SlideContent = styled.div`
  position: absolute;
  bottom: 8rem;
  width: 39rem;
`;

export const SlideTitle = styled.h3`
  color: white;
  font-family: ${props => props.theme.fonts.heading};
  font-size: 4.8rem;
  font-weight: 500;
  padding-bottom: 2rem;
  margin-bottom: 2rem;
  position: relative;
  
  &:before {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 12rem;
    height: 4px;
    background-color: ${props => props.theme.colors.primary}
  }
`;

export const SlideText = styled.div`
  color: white;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 2rem;
  font-weight: 400;
  line-height: 2.7rem;
`;